function createAnimation(image)
    local frames = {}
    local duration = 10
    local frameTime = 0
    local frameCount = 1
    local totalFrames = 0

    return {
        AddFrame = function(x, y, w, h)
            totalFrames = totalFrames + 1
            frames[totalFrames] = {
                duration = duration,
                quad = love.graphics.newQuad(x, y, w, h, image:getDimensions())
            }
        end,

        AddFrameWithData = function(data)
            local origin = nil
            local collider = nil

            for i = 1, #data.Properties do
                local property = data.Properties[i]

                if property.Name == "Collider" and collider == nil then
                    collider = property.Value
                elseif property.Name == "Origin" and origin == nil then
                    origin = property.Value
                end
            end

            local dim = data.Dimensions
            totalFrames = totalFrames + 1
            frames[totalFrames] = {
                origin = origin,
                dimension = dim,
                collider = collider,
                duration = data.Duration,
                quad = love.graphics.newQuad(dim.x, dim.y, dim.w, dim.h, image:getDimensions())
            }
        end,

        CurrentFrame = function()
            return frames[frameCount]
        end,

        Update = function(dt)
            if frameTime > (1 / duration) then
                frameCount = frameCount + 1
                if frameCount > totalFrames then
                    frameCount = 1
                    --duration = frames[frameCount].duration
                end
                frameTime = frameTime - (1 / duration)
            else
                frameTime = frameTime + dt
            end
        end,

        Draw = function(x, y, mirror)
            local offset = { x = 0, y = 0 }

            if frames[frameCount].origin ~= nil then
                offset = frames[frameCount].origin
            end

            if mirror then
                local _, _, w, _ = frames[frameCount].quad:getViewport()
                offset.x = w - offset.x
            end

            local xScale = mirror and -1 or 1
            love.graphics.draw(image, frames[frameCount].quad, x, y, 0, xScale, 1, offset.x, offset.y)
        end,

        Reset = function()
            frameCount = 1
        end
    }
end