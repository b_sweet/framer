require 'animateFactory'
local JSON = require 'json'
local Slab = require 'Slab.API'

local FileType = {
    TILESET = 1,
    ANIMATION = 2
}

local tileSet = {
    ImagePath = '',
    Frames = { }
}

local animation = {
    ImagePath = '',
    Types = { }
}

local WindowTypes = {
    TYPE = 1,
    ANIMATIONS = 2,
    FRAMES = 3,
    FRAMEDETAILS = 4,
    FRAMEPROPERTY = 5
}

local scale = 1
local anims = { }
local points = { }
local canvas = { }
local deltaTime = 0
local gridQuad = nil
local frameQuad = nil
local currImage = nil
local background = nil
local noAnimImage = nil
local mouseDown = false
local spriteSheet = { }
local loadDialog = false
local saveDialog = false
local selectedType = nil
local selectedFrame = nil
local titlebarOffset = 16
local propertyType = 'Bool'
local frameSelection = false
local selectedProperty = nil
local selectedAnimation = nil
local newTileSetDialog = false
local windowTitle = 'Animation'
local mouseActuallyDown = false
local imageW, imageH = 256, 256
local offset = { x = 0, y = 0 }
local windowW, windowH = 800, 600
local newAnimationSetDialog = false
local currentWindowType = WindowTypes.TYPE
local currentFileType = FileType.ANIMATION
local canvasColor = { r = 1, g = 1, b = 1 }
local originColor = { r = 1, g = 0, b = 0 }
local selectionColor = { r = 0, g = 0, b = 1 }
local endPos = { x = -1, y = -1, w = -1, h = -1 }
local startPos = { x = -1, y = -1, w = -1, h = -1 }
local withinWindow = { false, false, false, false }
local resetValue = { x = -1, y = -1, w = -1, h = -1 }
local backgroundColor = { r = 0.75, g = 0.75, b = 0.75 }
local propertyTypes = { 'Bool', 'Rect', 'Int', 'String', 'Position' }


-- Utility
local currType = function()
    return spriteSheet.Types[selectedType]
end

local currAnimation = function()
    if currentFileType == FileType.ANIMATION then
        return currType().Animations[selectedAnimation]
    else
        return spriteSheet
    end
end

local currFrame = function()
    return currAnimation().Frames[selectedFrame] 
end

local currProperty = function()
    return currFrame().Properties[selectedProperty]
end

local mouseInWindow = function()
    for i = 1, #withinWindow do
        if withinWindow[i] then return true end
    end

    return false
end

local getImageDimension = function()
    if currImage ~= nil then 
        return currImage:getDimensions() 
    end
    return imageW, imageH 
end

local updateCanvas = function()
    canvas = {
        x = offset.x,
        y = offset.y + titlebarOffset,
        w = imageW * scale,
        h = imageH * scale
    }
end

local calculateCanvasOffset = function(x, y)
    if canvas.w > windowW then
        local newX = offset.x + x

        if newX > 0 then
            newX = 0
        elseif newX < (windowW - canvas.w) then
            newX = windowW - canvas.w
        end

        offset.x = newX
    end

    if canvas.h > windowH then
        local newY = offset.y + y

        if newY > 0 then
            newY = 0
        elseif newY < (windowH - canvas.h) then
            newY = windowH - canvas.h
        end
    
        offset.y = newY
    end

    updateCanvas()
end

local addFrame = function(frame)
    local numFrames = #currAnimation().Frames + 1
    frame.Name = "Frame " .. tostring(numFrames)
    currAnimation().Frames[numFrames] = frame
end

local resetCanvas = function(x, y, w, h)
    frameQuad = love.graphics.newQuad(x, y, w, h, imageW, imageH)
    gridQuad = love.graphics.newQuad(0, 0, canvas.w, canvas.h, 2 * scale, 2 * scale)
end

local drawCanvas = function()
    if currImage ~= nil then
        love.graphics.draw(background, gridQuad, canvas.x, canvas.y, 0, 1, 1)
        love.graphics.draw(currImage, frameQuad, canvas.x, canvas.y, 0, scale, scale)
    end
end

local getGridPoint = function(x, y, w, h)
    updateCanvas()

    for i = 0, w do
        local minX = (i * scale) + canvas.x
        local maxX = minX + scale

        if x >= minX and x <= maxX then
            for j = 0, h do
                local minY = (j * scale) + canvas.y
                local maxY = minY + scale

                if y >= minY and y <= maxY then return minX, minY end
            end
        end
    end

    return x, y
end

local drawSelection = function()
    if frameSelection then
        local rect = {
            x = (startPos.x <= endPos.x) and startPos.x or endPos.x,
            y = (startPos.y <= endPos.y) and startPos.y or endPos.y,
            w = math.abs(endPos.x - startPos.x) + startPos.w,
            h = math.abs(endPos.y - startPos.y) + startPos.h
        }
    
        love.graphics.setColor(selectionColor.r, selectionColor.g, selectionColor.b, 0.25)
        love.graphics.rectangle("fill", rect.x, rect.y, rect.w, rect.h)
        
        love.graphics.setColor(selectionColor.r, selectionColor.g, selectionColor.b, 1)
        love.graphics.rectangle("line", rect.x, rect.y, rect.w, rect.h)
    elseif currentWindowType == WindowTypes.FRAMEDETAILS then
        if currFrame().Offset ~= nil then
            local _x, _y = (currFrame().Offset.x * scale) + offset.x, (currFrame().Offset.y * scale) + offset.y
            local x, y = getGridPoint(_x, _y + titlebarOffset, currFrame().Dimensions.w, currFrame().Dimensions.h)

            love.graphics.setColor(originColor.r, originColor.g, originColor.b, 0.25)
            love.graphics.rectangle("fill", x, y, scale, scale)
            
            love.graphics.setColor(originColor.r, originColor.g, originColor.b, 1)
            love.graphics.rectangle("line", x, y, scale, scale)
        end
    end
end

local setSelectionStart = function(x, y)
    local w, h = getImageDimension()
    local _x, _y = getGridPoint(x, y, w, h)
    startPos = { x = _x, y = _y, w = scale, h = scale }
    endPos = startPos
end

local setSelectionEnd = function(x, y)
    local w, h = getImageDimension()
    local _x, _y = getGridPoint(x, y, w, h) 
    endPos = { x =_x, y = _y, w = scale, h = scale }
end

local getSelectedFrame = function()
    local s = {
        x = startPos.x < endPos.x and startPos.x or endPos.x,
        y = startPos.y < endPos.y and startPos.y or endPos.y
    }
    local e = {
        x = startPos.x < endPos.x and endPos.x or startPos.x,
        y = startPos.y < endPos.y and endPos.y or startPos.y
    }

    local x1, y1 = (s.x - canvas.x) / scale, (s.y - canvas.y) / scale
    local x2, y2 = (e.x - canvas.x) / scale, (e.y - canvas.y) / scale

    return {
        Name = "",
        Dimensions = {
            x = x1, 
            y = y1,
            w = (x2 - x1) + 1,
            h = (y2 - y1) + 1,
        },
        Duration = 100,
        Offset = { x = 0, y = 0 },
        Properties = { }
    }
end

local resetSelectionPoints = function()
	endPos = resetValue
	startPos = resetValue
end

local checkMouseAgainstWindow = function(index)
    local windowW, windowH = Slab.GetWindowSize()
    local mouseX, mouseY = love.mouse.getPosition()
    local windowX, windowY = Slab.GetWindowPosition()
    local x = mouseX > (windowX - 3) and mouseX < (windowX + windowW)
    local y = mouseY > (windowY - 18) and mouseY < (windowY + windowH)

    withinWindow[index] = x and y
end

local loadSprite = function(aSpritePath, aObjectName)
    local file = assert(io.open(aSpritePath, "rb"))
    local filedata, err = love.filesystem.newFileData(file:read("*all"), aObjectName .. ".png")
    file:close()
    
    if (err == nil) then
        local imageData = love.image.newImageData(filedata)
        return love.graphics.newImage(imageData)
    end

    return nil
end

local saveFile = function(path)
    local fh = assert(io.open(path, "wb"))
        fh:write(JSON.encode(spriteSheet))
        fh:flush()
    fh:close()
end

local loadFile = function(path)
    local fh = assert(io.open(path, "r"))
        spriteSheet = JSON.decode(fh:read("*all"))

        if spriteSheet.Types ~= nil then
            currentFileType = FileType.ANIMATION
        else
            currentFileType = FileType.TILESET
            currentWindowType = WindowTypes.FRAMES
        end

        fh:flush()
    fh:close()
end

local createPreviewAnim = function(index)
    anims[index] = createAnimation(currImage)
    for i = 1, #currType().Animations[index].Frames do
        local quad = currType().Animations[index].Frames[i].Dimensions
        anims[index].AddFrame(quad.x, quad.y, quad.w, quad.h)
    end
end

local enableFrameSelection = function()
    frameSelection = true
    for i = 1, #withinWindow do
        withinWindow[i] = false
    end
end

local resetPropertyType = function()
    if currProperty().Value ~= nil then
        if currProperty().Value.w ~= nil then
            propertyType = 'Rect'
        elseif currProperty().Value.x ~= nil then
            propertyType = 'Position'
        elseif type(currProperty().Value) == 'boolean' then
            propertyType = 'Bool'
        elseif type(currProperty().Value) == 'string' then
            propertyType = 'String'
        elseif type(currProperty().Value) == 'number' then
            propertyType = 'Int'
        end
    else
        propertyType = 'Bool'
    end
end

--


-- UI
local toolbar = function()
	if Slab.BeginMainMenuBar() then
        -- Toolbar Start
		if Slab.BeginMenu("Create") then
            -- Toolbar Option Start
                if Slab.MenuItemChecked("Tile Set") then newTileSetDialog = true end
                if Slab.MenuItemChecked("Animation Set") then newAnimationSetDialog = true end
            -- Toolbar Option End
			Slab.EndMenu()
        end
		if Slab.BeginMenu("Edit") then
            -- Toolbar Option Start
                if Slab.MenuItemChecked("Save") then saveDialog = true end
                if Slab.MenuItemChecked("Load") then loadDialog = true end
            -- Toolbar Option End
			Slab.EndMenu()
        end
		if Slab.BeginMenu("System") then
            -- Toolbar Option Start
			    if Slab.MenuItem("Quit") then love.event.quit() end
            -- Toolbar Option End
			Slab.EndMenu()
        end
        
        -- Toolbar End
		Slab.EndMainMenuBar()
    end
end

local typeWindow = function()
    Slab.BeginListBox('TypeListBox', {W = 160})
    -- List Box Start
        for i = 1, #spriteSheet.Types, 1 do
            Slab.BeginListBoxItem('TypeListBox_Item_' .. i, {Selected = selectedType == i})
            -- Item Start
                Slab.Text(spriteSheet.Types[i].Name)
        
                if Slab.IsListBoxItemClicked() then
                    selectedType = i
                    selectedFrame = nil
                    selectedProperty = nil
                    selectedAnimation = nil
                end
            -- Item End
            Slab.EndListBoxItem()
        end
    -- List Box End
    Slab.EndListBox()
    
    Slab.Separator()

    Slab.BeginLayout("TypeButtonLayout", {AlignX = "center"})
    -- Button Layout Start
        if Slab.Button("+", {W = 25}) then
            local type = #spriteSheet.Types + 1
            spriteSheet.Types[type] = {
                Name = "Type " .. tostring(type),
                Animations = { }
            }
        end

        Slab.SameLine()

        if Slab.Button("x", {Disabled = selectedType == nil, W = 25}) then
            spriteSheet.Types[selectedType] = nil
            selectedType = nil
        end
        
        Slab.SameLine()

        if Slab.Button("Edit", {Disabled = selectedType == nil, W = 100}) then
            anims = { }
            currentWindowType = WindowTypes.ANIMATIONS
        end
    -- Button Layout End
    Slab.EndLayout()
end

local animationWindow = function()
    Slab.BeginLayout("TypeName", {AlignX = "center"})
    -- Name Layout Start
        Slab.Text("Name: ")
        Slab.SameLine()
        if Slab.Input('MyInput', {W = 100, ReturnOnText = false, Text = currType().Name}) then
            currType().Name = Slab.GetInputText()
        end
    -- Name Layout End
    Slab.EndLayout()
    
    Slab.Separator()

    Slab.BeginListBox('AnimationListBox', {W = 160})
    -- List Box Start
        for i = 1, #currType().Animations, 1 do
            Slab.BeginListBoxItem('AnimationListBox_Item_' .. i, {Selected = selectedAnimation == i})
            -- Item Start
                Slab.BeginLayout("AnimListItem", {AlignX = "center"})
                -- Anim List Item Layout Start
                    local imageName = ''
                    local imageData = { }

                    if #currType().Animations[i].Frames == 0 then
                        imageName = 'NoAnimPreview'
                        imageData = {
                            Image = noAnimImage,
                            Tooltip = currType().Animations[i].Name
                        }
                    else
                        if anims[i] == nil then createPreviewAnim(i) end

                        anims[i].Update(deltaTime)

                        local x, y, w, h = anims[i].CurrentFrame().quad:getViewport()
                        imageName = 'AnimPreview'
                        imageData = {
                            Image = currImage,
                            SubX = x,
                            SubY = y,
                            SubW = w,
                            SubH = h,
                            Tooltip = currType().Animations[i].Name
                        }
                    end

                    -- Draw Animation
                    Slab.Image(imageName, imageData)

                -- Anim List Item Layout Start
                Slab.EndLayout()
        
                if Slab.IsListBoxItemClicked() then
                    selectedAnimation = i
                    selectedFrame = nil
                    selectedProperty = nil
                end
            -- Item End
            Slab.EndListBoxItem()
        end
    -- List Box End
    Slab.EndListBox()

    Slab.Separator()

    Slab.BeginLayout("AnimationButtonLayout", {AlignX = "center"})
    -- Button Layout Start
        if Slab.Button("+", {W = 25}) then
            local anim = #currType().Animations + 1
            currType().Animations[anim] = {
                Name = "Animation " .. tostring(anim),
                Frames = { }
            }

            anims[#anims + 1] = nil
        end

        Slab.SameLine()

        if Slab.Button("x", {Disabled = selectedAnimation == nil, W = 25}) then
            currType().Animations[selectedAnimation] = nil
            selectedAnimation = nil
        end

        Slab.SameLine()

        if Slab.Button("Edit", {Disabled = selectedAnimation == nil, W = 100}) then
            currentWindowType = WindowTypes.FRAMES
        end

    -- Button Layout End
    Slab.EndLayout()
end

local frameWindow = function()
    Slab.BeginLayout("AnimationName", {AlignX = "center"})
    -- Name Layout Start
        Slab.Text("Name: ")
        Slab.SameLine()
        if Slab.Input('MyInput', {W = 100, ReturnOnText = false, Text = currAnimation().Name}) then
            currAnimation().Name = Slab.GetInputText()
        end
    -- Name Layout End
    Slab.EndLayout()
    
    Slab.Separator()

    Slab.BeginListBox('FrameListBox', {W = 160})
    -- List Box Start
        for i = 1, #currAnimation().Frames, 1 do
            Slab.BeginListBoxItem('FrameListBox_Item_' .. i, {Selected = selectedFrame == i})
            -- Item Start
                Slab.BeginLayout("FrameListItem", {AlignX = "center"})
                -- Frame List Item Layout Start
                    local dim = currAnimation().Frames[i].Dimensions
                    Slab.Image("FramePreview", {
                        Image = currImage,
                        SubX = dim.x,
                        SubY = dim.y,
                        SubW = dim.w,
                        SubH = dim.h,
                        Tooltip = currAnimation().Frames[i].Name
                    })

                -- Frame List Item Layout Start
                Slab.EndLayout()
                if Slab.IsListBoxItemClicked() then
                    selectedFrame = i
                    selectedProperty = nil
                end
            -- Item End
            Slab.EndListBoxItem()
        end
    -- List Box End
    Slab.EndListBox()
    
    Slab.Separator()

    Slab.BeginLayout("FrameButtonLayout", {AlignX = "center"})
    -- Button Layout Start
        if Slab.Button("+", {W = 25}) then
            enableFrameSelection()
        end

        Slab.SameLine()

        if Slab.Button("x", {Disabled = selectedFrame == nil, W = 25}) then
            currAnimation().Frames[selectedFrame] = nil
            selectedFrame = nil
        end

        Slab.SameLine()

        if Slab.Button("Edit", {Disabled = selectedFrame == nil, W = 100}) then
            currentWindowType = WindowTypes.FRAMEDETAILS
            
            local dims = currFrame().Dimensions
            resetCanvas(dims.x, dims.y, dims.w, dims.h)
        end
    -- Button Layout End
    Slab.EndLayout()
end

local valueWindow = function()
    local frame = currFrame()
    Slab.BeginLayout("Frame", {AlignRowY = "center"})
    -- Layout Start
        -- Start Name
            Slab.BeginLayout("FrameName", {AlignX = "center"})
            -- Frame Name Layout Start
                Slab.Text("Name: ")
                Slab.SameLine()
                if Slab.Input('MyInput', {W = 100, ReturnOnText = false, Text = frame.Name}) then
                    currFrame().Name = Slab.GetInputText()
                end
            -- Frame Name Layout End
            Slab.EndLayout()
        -- End Name
        Slab.Separator()
        -- Start Image
            Slab.BeginLayout("Image", {AlignX = "center"})
            -- Image Layout Begin
                Slab.Image('FrameImage', {
                    Image = currImage,
                    SubX = frame.Dimensions.x,
                    SubY = frame.Dimensions.y,
                    SubW = frame.Dimensions.w,
                    SubH = frame.Dimensions.h
                })
            -- Value Layout End
            Slab.EndLayout()
        -- End Image
        Slab.Separator()
        -- Start Duration
            Slab.BeginLayout("FrameDuration", {AlignRowY = "left", Columns = 2})
            -- Frame Duration Layout Start
		        Slab.SetLayoutColumn(1)
                    Slab.Text("Duration (ms): ")
                Slab.SetLayoutColumn(2)
                    if Slab.Input('Duration', {W = 50, Text = tostring(frame.Duration), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Duration = Slab.GetInputNumber()
                    end
            -- Frame Duration Layout End
            Slab.EndLayout()

        -- End Duration
        Slab.Separator()
        -- Start Offset
            Slab.BeginLayout("FrameOffsetLabel", {AlignX = "left"})
            -- Offset Label Layout Start
                Slab.Text("Offset: ")
            -- Offset Label Layout End
            Slab.EndLayout()
                
            Slab.BeginLayout("FrameOffset", {AlignX = "left", Columns = 2})
            -- Offset Layout Start
                if frame.Offset == nil then
                    frame.Offset = { x = 0, y = 0 }
                end

                Slab.SetLayoutColumn(1)
                    Slab.Text("X:")
                    Slab.SameLine()
                    --x
                    if Slab.Input('XOffsetInput', {W = 50, Text = tostring(frame.Offset.x), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Offset.x = Slab.GetInputNumber()
                    end

                Slab.SetLayoutColumn(2)
                    Slab.Text("Y:")
                    Slab.SameLine()
                    --y
                    if Slab.Input('YOffsetInput', {W = 50, Text = tostring(frame.Offset.y), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Offset.y = Slab.GetInputNumber()
                    end
            -- Offset Layout End
            Slab.EndLayout()
        -- End Offset
        Slab.Separator()
        -- Start Dimensions

            Slab.BeginLayout("FrameDimensionsLabel", {AlignX = "left"})
            -- Dimensions Label Layout Start
                Slab.Text("Dimensions: ")
            -- Dimensions Label Layout End
            Slab.EndLayout()
                
            Slab.BeginLayout("FrameDimensions", {AlignX = "left", Columns = 4})
            -- Dimension Layout Start
		        Slab.SetLayoutColumn(1)
                    Slab.Text("X:")
                    Slab.Text("W:")

		        Slab.SetLayoutColumn(2)
                    if Slab.Input('XInput', {W = 50, Text = tostring(frame.Dimensions.x), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Dimensions.x = Slab.GetInputNumber()
                    end

                    if Slab.Input('WInput', {W = 50, Text = tostring(frame.Dimensions.w), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Dimensions.w = Slab.GetInputNumber()
                    end

		        Slab.SetLayoutColumn(3)
                    Slab.Text("Y:")
                    Slab.Text("H:")

                Slab.SetLayoutColumn(4)
                    if Slab.Input('YInput', {W = 50, Text = tostring(frame.Dimensions.y), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Dimensions.y = Slab.GetInputNumber()
                    end

                    if Slab.Input('HInput', {W = 50, Text = tostring(frame.Dimensions.h), ReturnOnText = true, NumbersOnly = true}) then
                        frame.Dimensions.h = Slab.GetInputNumber()
                    end

            -- Dimension Layout End
            Slab.EndLayout()
        
        -- End Dimensions
        Slab.Separator()
        -- Start Properties
            Slab.BeginLayout("PropertiesTitle", {AlignX = "center"})
            -- Properties Layout Start
                Slab.Text("Properties")

                Slab.BeginListBox('PropertiesListBox', {W = 160})
                -- List Box Start
                    for i = 1, #frame.Properties, 1 do
                        Slab.BeginListBoxItem('PropertiesListBox_Item_' .. i, {Selected = selectedProperty == i})
                        -- Item Start
                            Slab.Text(frame.Properties[i].Name)
                    
                            if Slab.IsListBoxItemClicked() then
                                selectedProperty = i
                            end
                        -- Item End
                        Slab.EndListBoxItem()
                    end
                -- List Box End
                Slab.EndListBox()

                Slab.BeginLayout("TypeButtonLayout", {AlignX = "center"})
                -- Button Layout Start
                    if Slab.Button("+", {W = 25}) then
                        local count = #frame.Properties + 1
                        frame.Properties[count] = {
                            Name = "Property " .. tostring(count),
                            Value = nil
                        }
                    end

                    Slab.SameLine()

                    if Slab.Button("x", {Disabled = selectedProperty == nil, W = 25}) then
                        frame.Properties[selectedProperty] = nil
                        selectedProperty = nil
                    end

                    Slab.SameLine()
                
                    if Slab.Button("Edit", {Disabled = selectedProperty == nil, W = 100}) then
                        currentWindowType = WindowTypes.FRAMEPROPERTY
                        resetPropertyType()
                    end

                -- Button Layout End
                Slab.EndLayout()
            -- Properties Layout End
            Slab.EndLayout()
        -- End Properties
    -- Layout End
    Slab.EndLayout()
end

local propertyWindow = function()
    Slab.BeginLayout("PropertyName", {AlignX = "center"})
    -- Name Layout Start
        Slab.Text("Name: ")
        Slab.SameLine()
        if Slab.Input('MyInput', {W = 100, ReturnOnText = false, Text = currProperty().Name}) then
            currProperty().Name = Slab.GetInputText()
        end
    -- Name Layout End
    Slab.EndLayout()

    Slab.Separator()

    Slab.BeginLayout("PropertyTypeLayout", {AlignX = "center"})
    -- Layout Start
        if Slab.BeginComboBox('PropertyTypeComboBox', {Selected = propertyType}) then
            for I, V in ipairs(propertyTypes) do
                if Slab.TextSelectable(V) then
                    propertyType = V
                    currProperty().Value = nil
                end
            end
            Slab.EndComboBox()
        end

        Slab.Separator()
        
        if propertyType == 'Rect' then
            if currProperty().Value == nil then
                currProperty().Value = {
                    isCollider = false,
                    Value = {
                        x = 0, y = 0, w = 0, h = 0
                    }
                } 
            end
                
            if Slab.CheckBox(currProperty().Value.isCollider, "Is Collider") then 
                currProperty().Value.isCollider = not currProperty().Value.isCollider
            end

            Slab.Separator()

            Slab.BeginLayout("PropertyRectLayout", {AlignX = "left", Columns = 4})
            -- Rect Layout Start
                Slab.SetLayoutColumn(1)
                    Slab.Text("X:")
                    Slab.Text("W:")

                Slab.SetLayoutColumn(2)
                    if Slab.Input('XInput', {W = 50, Text = tostring(currProperty().Value.x), ReturnOnText = true, NumbersOnly = true}) then
                        currProperty().Value.x = Slab.GetInputNumber()
                    end

                    if Slab.Input('WInput', {W = 50, Text = tostring(currProperty().Value.w), ReturnOnText = true, NumbersOnly = true}) then
                        currProperty().Value.w = Slab.GetInputNumber()
                    end

                Slab.SetLayoutColumn(3)
                    Slab.Text("Y:")
                    Slab.Text("H:")

                Slab.SetLayoutColumn(4)
                    if Slab.Input('YInput', {W = 50, Text = tostring(currProperty().Value.y), ReturnOnText = true, NumbersOnly = true}) then
                        currProperty().Value.y = Slab.GetInputNumber()
                    end

                    if Slab.Input('HInput', {W = 50, Text = tostring(currProperty().Value.h), ReturnOnText = true, NumbersOnly = true}) then
                        currProperty().Value.h = Slab.GetInputNumber()
                    end
            -- Rect Layout End
            Slab.EndLayout()
            
            Slab.BeginLayout("PropertyRectSelectLayout", {AlignX = "center"})
            -- Button Layout Start
                if Slab.Button("Select", {W = 160}) then
                    enableFrameSelection()
                end
            -- Button Layout End
            Slab.EndLayout()
        elseif propertyType == 'Bool' then
            if currProperty().Value == nil then currProperty().Value = false end
            if Slab.CheckBox(currProperty().Value, "Value") then 
                currProperty().Value = not currProperty().Value 
            end
        elseif propertyType == 'Int' then
            if currProperty().Value == nil then currProperty().Value = 0 end
            if Slab.Input('StringInput', {Text = tostring(currProperty().Value), ReturnOnText = true, NumbersOnly = true}) then 
                currProperty().Value = tonumber(Slab.GetInputText())
            end
        elseif propertyType == 'String' then
            if currProperty().Value == nil then currProperty().Value = '' end
            if Slab.Input('StringInput', {Text = currProperty().Value, ReturnOnText = true}) then 
                currProperty().Value = Slab.GetInputText()
            end
        elseif propertyType == 'Position' then
            Slab.BeginLayout("PropertyPositionLayout", {AlignX = "left", Columns = 2})
            -- Position Layout Start
                if currProperty().Value == nil then
                    currProperty().Value = { x = 0, y = 0 } 
                end
                Slab.SetLayoutColumn(1)
                    Slab.Text("X:")
                    Slab.SameLine()
                    --x
                    if Slab.Input('XInput', {W = 50, Text = tostring(currProperty().Value.x), ReturnOnText = true, NumbersOnly = true, ReadOnly = frameSelection}) then
                        currProperty().Value.x = Slab.GetInputNumber()
                    end
    
                Slab.SetLayoutColumn(2)
                    Slab.Text("Y:")
                    Slab.SameLine()
                    --y
                    if Slab.Input('YInput', {W = 50, Text = tostring(currProperty().Value.y), ReturnOnText = true, NumbersOnly = true, ReadOnly = frameSelection}) then
                        currProperty().Value.y = Slab.GetInputNumber()
                    end
            -- Position Layout End
            Slab.EndLayout()
        end
    -- Layout End
    Slab.EndLayout()
end

local drawDetails = function()
    if (not frameSelection) and (currImage ~= nil) then
        Slab.BeginWindow('Window', {AllowMove = false, X = windowW - 168, Y = titlebarOffset, Title = windowTitle})
        checkMouseAgainstWindow(1)
        -- Window Start
        if currentFileType == FileType.ANIMATION then
            if currentWindowType == WindowTypes.TYPE then
                windowTitle = "Types"
                typeWindow()
            elseif currentWindowType == WindowTypes.ANIMATIONS then
                windowTitle = "Type: " .. currType().Name
                animationWindow()
            end
        end

        if currentWindowType == WindowTypes.FRAMES then
            if currentFileType == FileType.ANIMATION then
                windowTitle = "Animation: " .. currAnimation().Name
            else
                windowTitle = "Tile Set"
            end
            frameWindow()
        elseif currentWindowType == WindowTypes.FRAMEDETAILS then
            windowTitle = "Frame: " .. currFrame().Name
            valueWindow()
        elseif currentWindowType == WindowTypes.FRAMEPROPERTY then
            windowTitle = "Property: " .. currProperty().Name
            propertyWindow()
        end

        if currentWindowType ~= (currentFileType == FileType.ANIMATION and WindowTypes.TYPE or WindowTypes.FRAMES) then
            Slab.Separator()
            
            Slab.BeginLayout("BackButton", {AlignX = "center"})
            -- Back Button Layout Start
                if Slab.Button("Back") then
                    currentWindowType = currentWindowType - 1

                    if currentWindowType == WindowTypes.FRAMES then
                        resetCanvas(0, 0, imageW, imageH)
                    elseif currentWindowType == WindowTypes.ANIMATIONS then
                        createPreviewAnim(selectedAnimation)
                    end
                end
            -- Back Button Layout End
            Slab.EndLayout()
        end
        
        -- Window End
        Slab.EndWindow()
    end
end

local loadFileDialog = function ()
    if loadDialog then
        local windowIndex = 2
        local Result = Slab.FileDialog({Type = 'openfile', Filters = {'.json', 'Spritesheet Data'}})
        
        checkMouseAgainstWindow(windowIndex)

        if Result.Button ~= "" then
            loadDialog = false
            withinWindow[windowIndex] = false

            if Result.Button ~= 'Cancel' then
                loadFile(Result.Files[1])
                
                if spriteSheet.ImagePath ~= '' then
                    currImage = loadSprite(spriteSheet.ImagePath, "image")
                    currImage:setFilter("nearest", "nearest")
                    imageW, imageH = currImage:getDimensions()

                    updateCanvas()

                    resetCanvas(0, 0, imageW, imageH)
                end
            end
        end
    end
end

local animationSetDialog = function ()
    if newAnimationSetDialog then
        local windowIndex = 3
        local Result = Slab.FileDialog({Type = 'openfile', Filters = {'.png', 'Spritesheet Image'}})
    
        checkMouseAgainstWindow(windowIndex)

        if Result.Button ~= "" then
            newAnimationSetDialog = false
            withinWindow[windowIndex] = false
            
            if Result.Button ~= 'Cancel' then
                spriteSheet = animation

                currentFileType = FileType.ANIMATION

                spriteSheet.ImagePath = Result.Files[1]
                currImage = loadSprite(spriteSheet.ImagePath, "image")
                currImage:setFilter("nearest", "nearest")
                imageW, imageH = currImage:getDimensions()

                updateCanvas()

                resetCanvas(0, 0, imageW, imageH)
            end
        end
    end
end

local tileSetDialog = function ()
    if newTileSetDialog then
        local windowIndex = 4
        local Result = Slab.FileDialog({Type = 'openfile', Filters = {'.png', 'Spritesheet Image'}})
    
        checkMouseAgainstWindow(windowIndex)

        if Result.Button ~= "" then
            newTileSetDialog = false
            withinWindow[windowIndex] = false
            
            if Result.Button ~= 'Cancel' then
                spriteSheet = tileSet

                currentFileType = FileType.TILESET
                currentWindowType = WindowTypes.FRAMES

                spriteSheet.ImagePath = Result.Files[1]
                currImage = loadSprite(spriteSheet.ImagePath, "image")
                currImage:setFilter("nearest", "nearest")
                imageW, imageH = currImage:getDimensions()

                updateCanvas()

                resetCanvas(0, 0, imageW, imageH)
            end
        end
    end
end

local saveFileDialog = function ()
    if saveDialog then
        local windowIndex = 4
        local Result = Slab.FileDialog({Type = 'savefile', Filters = {'.json', 'Spritesheet Data'}})

        checkMouseAgainstWindow(windowIndex)

        if Result.Button ~= "" then
            saveDialog = false
            withinWindow[windowIndex] = false
            if Result.Button ~= 'Cancel' then
                saveFile(Result.Files[1])
            end
        end
    end
end

local updateUI = function()
    toolbar()
    drawDetails()
    tileSetDialog()
    loadFileDialog()
    saveFileDialog()
    animationSetDialog()
end
--


-- LOVE2D
function love.draw()
    drawCanvas()
    drawSelection()
    Slab.Draw()
end

function love.load(args)
    love.window.setTitle("Framer")
    love.graphics.setBackgroundColor(backgroundColor.r, backgroundColor.g, backgroundColor.b)

    noAnimImage = love.graphics.newImage("no_anim.png")

    background = love.graphics.newImage("tiled.png")
    background:setWrap('repeat', 'repeat', 'repeat')
    background:setFilter('nearest', 'nearest')

    updateCanvas()

	Slab.Initialize(args)
end

function love.update(dt)
    Slab.Update(dt)
    updateUI()

    deltaTime = dt
end

function love.resize(w, h)
    windowW = w
    windowH = h
end

function love.wheelmoved(x, y)
    if y ~= 0 and not mouseInWindow() and not mouseActuallyDown then
        local newScale = scale + y
        if newScale < 1 then newScale = 1 end
        if newScale > 100 then newScale = 100 end
        scale = newScale

        updateCanvas()

        gridQuad = love.graphics.newQuad(0, 0, canvas.w, canvas.h, 2 * scale, 2 * scale)
    end
end

function love.mousepressed(x, y, button)
    mouseActuallyDown = true
    if button == 1 and not mouseInWindow() then
        if frameSelection then
            setSelectionStart(x, y)
        end
    elseif button == 2 then
        mouseDown = true
	end
end

function love.mousereleased(x, y, button)
    mouseActuallyDown = false
    if frameSelection then
        if currentWindowType == WindowTypes.FRAMEPROPERTY then
            currProperty().Value = getSelectedFrame().Dimensions
        else
            addFrame(getSelectedFrame())
        end

        frameSelection = false
    end
    
    mouseDown = false

    resetSelectionPoints()
end

function love.mousemoved(x, y, dx, dy, istouch)
    if frameSelection and startPos ~= resetValue then
        setSelectionEnd(x, y)
    elseif mouseDown then
        calculateCanvasOffset(dx, dy)
	end
end
--