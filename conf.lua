function love.conf(t) 
    t.console = true
    t.version = "11.3"
    t.window.minwidth = 800
    t.window.minheight = 600
    t.window.resizable = true
end